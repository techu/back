var express = require('express'),
  app = express(),
  // cors = require('cors'),
  bodyParser = require('body-parser'),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE'); 
  next();
});

var requestJson = require('request-json');
var urlMlabClients = "https://api.mlab.com/api/1/databases/bdbanca/collections/clients";
var urlMlabAccounts = "https://api.mlab.com/api/1/databases/bdbanca/collections/accounts";
var urlMlabTransactions = "https://api.mlab.com/api/1/databases/bdbanca/collections/transactions";
var mlabApyKey = "?apiKey=MeiVpeZEipyAl58H4hyl-Gmvdr0ghXk4";

app.listen(port);

console.log('BBVA RESTful API server started on: ' + port);


app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.send('BBVA API </br></br>/login POST </br>/clients GET </br>/clients/&lt;id&gt; PUT/DELETE </br>/transactions GET </br>/transactions/&lt;id&gt; PUT/DELETE </br>/transactions/&lt;idclient&gt; GET');
});


//
// login
//
app.post('/login', function(req, res) {
  console.log("/login body: " + JSON.stringify(req.body));

  var params = '&q={"email":"' + req.body.email + '","password":"' + req.body.password + '"}';

  var client = requestJson.createClient(urlMlabClients + mlabApyKey + params);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Login success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});


//
// clients
//
app.get('/clients',  function(req, res) {
    console.log("/clients get with params: " + JSON.stringify(req.params));

    var client = requestJson.createClient(urlMlabClients + mlabApyKey);

    client.get('', function(err, resMlab, body) {
        if (err) {
          console.log("Error: " + err + " body: " + body);
          res.send(err);
        }
        else {
          console.log("Get clients success, body: " + JSON.stringify(body));
          res.send(body);
        }
    });
});

app.post('/clients', function(req, res) {  
  console.log("/clients post body: " + JSON.stringify(req.body));

  // check is email already exists
  var params = '&q={"email":"' + req.body.email + '"}';

  var client = requestJson.createClient(urlMlabClients + mlabApyKey + params);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        if (body.length == 0) {
          console.log("Email not found, adding new user");
          
          // add client
          client = requestJson.createClient(urlMlabClients + mlabApyKey);

          client.post('', req.body, function(err, resMlab, body) {
              if (err) {
                console.log("Error: " + err + " body: " + body);
                res.send(err);
              }
              else {
                console.log("Post clients success, body: " + JSON.stringify(body));
                res.send(body);
              }
          });
        } 
        else {
          console.log("Email already exists: " + JSON.stringify(body));
          res.send(body);
        }
      }
  });
});

app.put('/clients/:idclient', function(req, res) {  
  console.log("/clients put idclient " + req.params.idclient + " body: " + JSON.stringify(req.body));

  var url = urlMlabClients + '/' + req.params.idclient + '/' + mlabApyKey;

  console.log('url: ' + url);

  var client = requestJson.createClient(url);

  client.put('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

// app.options('/clients/:idclient', cors()); // enable pre-flight request for DELETE request
app.delete('/clients/:idclient', function(req, res) {
    console.log("/clients delete idclient: " + req.params.idclient);

    var url = urlMlabClients + '/' + req.params.idclient + '/' + mlabApyKey;

    console.log('url: ' + url);

    var client = requestJson.createClient(url);

    client.delete('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Delete clients success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});



//
// accounts
//
app.get('/accounts/:idclient', function(req, res) {
  console.log("/accounts get with idclient: " + req.params.idclient);

  var params = '&q={"idclient":"' + req.params.idclient + '"}';

  var url = urlMlabAccounts + mlabApyKey + params;

  var client = requestJson.createClient(urlMlabAccounts + mlabApyKey + params);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

app.get('/accounts',  function(req, res) {
    console.log("/accounts get with params: " + JSON.stringify(req.params));

    var account = requestJson.createClient(urlMlabAccounts + mlabApyKey);

    account.get('', function(err, resMlab, body) {
        if (err) {
          console.log("Error: " + err + " body: " + body);
          res.send(err);
        }
        else {
          console.log("Get accounts success, body: " + JSON.stringify(body));
          res.send(body);
        }
    });
});

app.post('/accounts', function(req, res) {  
  console.log("/accounts post body: " + JSON.stringify(req.body));

  // check is email already exists
  var params = '&q={"iban":"' + req.body.iban + '"}';

  var account = requestJson.createClient(urlMlabAccounts + mlabApyKey + params);

  account.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        if (body.length == 0) {
          console.log("Iban not found, adding new account");
          
          // add account
          account = requestJson.createClient(urlMlabAccounts + mlabApyKey);

          account.post('', req.body, function(err, resMlab, body) {
              if (err) {
                console.log("Error: " + err + " body: " + body);
                res.send(err);
              }
              else {
                console.log("Post accounts success, body: " + JSON.stringify(body));
                res.send(body);
              }
          });
        } 
        else {
          console.log("Iban already exists: " + JSON.stringify(body));
          res.send(body);
        }
      }
  });
});

app.put('/accounts/:idaccount', function(req, res) {  
  console.log("/accounts put idaccount " + req.params.idaccount + " body: " + JSON.stringify(req.body));

  var url = urlMlabAccounts + '/' + req.params.idaccount + '/' + mlabApyKey;

  console.log('url: ' + url);

  var account = requestJson.createClient(url);

  account.put('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

// app.options('/accounts/:idaccount', cors()); // enable pre-flight request for DELETE request
app.delete('/accounts/:idaccount', function(req, res) {
    console.log("/accounts delete idaccount: " + req.params.idaccount);

    var url = urlMlabAccounts + '/' + req.params.idaccount + '/' + mlabApyKey;

    console.log('url: ' + url);

    var account = requestJson.createClient(url);

    account.delete('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Delete accounts success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});


//
// transactions
//
app.get('/transactions/account/:idaccount', function(req, res) {
  console.log("/transactions get with idaccount: " + req.params.idaccount);

  var params = '&q={"idaccount":"' + req.params.idaccount + '"}';

  var url = urlMlabTransactions + mlabApyKey + params;

  var client = requestJson.createClient(urlMlabTransactions + mlabApyKey + params);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

app.get('/transactions/:idclient', function(req, res) {
  console.log("/transactions get with idclient: " + req.params.idclient);

  var params = '&q={"idclient":"' + req.params.idclient + '"}';

  var url = urlMlabTransactions + mlabApyKey + params;

  var client = requestJson.createClient(urlMlabTransactions + mlabApyKey + params);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

app.get('/transactions', function(req, res) {
  console.log("/transactions get");

  var client = requestJson.createClient(urlMlabTransactions + mlabApyKey);

  client.get('', function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

app.post('/transactions', function(req, res) {  
  console.log("/transactions post body: " + JSON.stringify(req.body));

  var client = requestJson.createClient(urlMlabTransactions + mlabApyKey);

  client.post('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

app.put('/transactions/:idtransaction', function(req, res) {  
  console.log("/transactions put idtransaction " + req.params.idtransaction + " body: " + JSON.stringify(req.body));

  var url = urlMlabTransactions + '/' + req.params.idtransaction + '/' + mlabApyKey;

  console.log('url: ' + url);

  var client = requestJson.createClient(url);

  client.put('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});

// app.options('/clients/:idclient', cors()); // enable pre-flight request for DELETE request
app.delete('/transactions/:idtransaction', function(req, res) {
    console.log("/transactions delete idtransaction " + req.params.idtransaction);

    var url = urlMlabTransactions + '/' + req.params.idtransaction + '/' + mlabApyKey;

    console.log('url: ' + url);

    var client = requestJson.createClient(url);

    client.delete('', req.body, function(err, resMlab, body) {
      if (err) {
        console.log("Error: " + err + " body: " + body);
        res.send(err);
      }
      else {
        console.log("Delete transactions success, body: " + JSON.stringify(body));
        res.send(body);
      }
  });
});
